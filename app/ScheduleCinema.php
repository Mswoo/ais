<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ScheduleCinema extends Model
{
    public function scopeFilter($query, $filters)
    {
        return $filters->apply($query);
    }
   
}