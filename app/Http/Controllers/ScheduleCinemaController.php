<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Modules\Filter\ScheduleCinemaFilters;
use App\Modules\Parsers\ScheduleParser;
use App\ScheduleCinema;

/**
 * Class ScheduleCinemaController
 * @package App\Http\Controllers
 */
class ScheduleCinemaController extends Controller
{
    //

    /**
     * @param ScheduleCinemaFilters $filters
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(ScheduleCinemaFilters $filters)
    {
        $schedules = ScheduleCinema::filter($filters)->orderBy('date', 'asc')->get();

        return view('schedule', compact('schedules'));
    }

    /**
     *
     * Обновление расписания
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function update()
    {

        $link = "http://cityopen.ru/afisha/kinoteatr-salavat/";

        $parser = new ScheduleParser($link);
        $schedules = $parser->run()->getSchedules();

        if (!empty($schedules)) {

            $countI = 0;
            $countF = 0;

            foreach ($schedules as $schedule) {
                //сделаем проверку на существование записи в бд

                $scheduleCinema =
                    ScheduleCinema::where('date', '=', $schedule['date'])
                        ->where('start_session', '=', $schedule['time'])
                        ->where('name', '=', $schedule['name'])
                        ->get();

                if ($scheduleCinema->isEmpty()) {
                    $scheduleCinema = new ScheduleCinema();
                    $scheduleCinema->date = $schedule['date'];
                    $scheduleCinema->start_session = $schedule['time'];
                    $scheduleCinema->name = $schedule['name'];
                    $scheduleCinema->age_limit = $schedule['age'];
                    $scheduleCinema->price = $schedule['price'];

                    $scheduleCinema->save();
                    $countI++;
                    continue;
                }

                $countF++;

            }
        }

        return view('update', compact('countI', 'countF'));
    }

}
