<?php 


namespace App\Modules\Filter;

use Illuminate\Http\Request;

/**
 * 
 * abstract class QueryFilter
 * 
 */
abstract class QueryFilter
{

	protected $query;

	protected $request;
	
	/**
	 * 
	 * @param Request $request
	 */
	public function __construct(Request $request)
	{
		$this->request = $request;
	}

    /**
     * Применяем все доступные фильтры
     * @param  [object] $query Входяший запрос в бд
     * @return [object]   $query     Запрос обработаный фильтрами
     */
	public function apply($query)
	{
		$this->query = $query;

		foreach ($this->request->all() as $filter => $value) {

			if(method_exists($this, $filter) and !empty($value)) {

				$this->query = $this->{$filter}($value);

			}		
			
		}

		return $this->query;
	}



}