<?php 

namespace App\Modules\Filter;

use App\Modules\Filter\QueryFilter;

/**
 * 
 * Класс фильтрации расписания
* 
*/
class ScheduleCinemaFilters extends QueryFilter
{

    /**
     * Фильтрация по дате
     * @param  [date] $date Дата сеанса
     * @return  [object]    $query
     */
    protected function date($date)
    {
        return $this->query->where('date', $date);
    }

    /**
     * Фильтрация по цене от значения
     * @param  [int] $price Цена от
     * @return  [object]    $query
     */
    protected function priceTo($price)
    {
        return $this->query->where('price', '>=', $price);
    }
    
    /**
     * Фильтрация по цене до значения
     * @param  [int] $price Цена от
     * @return [object]    $query
     */
    protected function priceDo($price)
    {
        return $this->query->where('price', '<=', $price);
    }
    
    /**
     * Фильтрация по времени сеанса от значения
     * @param  [int] $time Время от
     * @return [object]    $query
     */
    protected function timeTo($time)
    {
        return $this->query->where('start_session', '>=', $time);
    }
    
    /**
     * Фильтрация по времени сеанса до значения
     * @param  [int] $time время от
     * @return [object]    $query
     */
    protected function timeDo($time)
    {
        return $this->query->where('start_session', '<=', $time);
    }
	
	
}