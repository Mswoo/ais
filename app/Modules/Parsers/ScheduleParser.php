<?php

namespace App\Modules\Parsers;

use Symfony\Component\DomCrawler\Crawler;

/**
 *
 * Salavat Parser
 *
 * @property  crawler
 */
class ScheduleParser
{

    /**
     * @var Crawler
     */
    protected $crawler;

    /**
     * @var
     */
    protected $link;

    /**
     * @var
     */
    protected $schedules;

    /**
     * @var array
     */
    protected $stopListArray = [
        'ЗЕЛЁНЫЙ ЗАЛ',
        'КРАСНЫЙ ЗАЛ',
        'Начало',
        'Название',
        'Афиша Кинотеатра Салават',
        ''
    ];

    /**
     * @var string
     */
    protected $year = '2019';

    /**
     * ScheduleParser constructor.
     * @param $link
     */
    public function __construct($link)
    {
        $this->crawler = new Crawler(null, $link);
        $this->link = $link;

        return $this;

    }

    /**
     *
     * Запуск парсера
     *
     * @return $this
     */
    public function run()
    {
        $html = file_get_contents($this->link);

        $crawler = $this->getArrayToHtml($html);

        $date = date('Y-m-d');

        foreach ($crawler as $c => $v) {

            $schedule = explode(PHP_EOL, $v);
            $schedule = $this->clearArray($schedule, $this->stopListArray);

            if (empty($schedule)) {
                continue;
            }


            $count = count($schedule);

            switch ($count) {
                case 1 :  // Если 1 то это всего скорее дата
                    $date = $this->getDate($schedule);
                    break;
                case 4 :  // Ксли 4 то это строка с расписанием
                    $this->schedules[] = [
                        'date'  => $date,
                        'time'  => $schedule[0] . ':00',
                        'name'  => strip_tags($schedule[1]),
                        'age'   => $schedule[2],
                        'price' => intval($schedule[3])
                    ];
                    break;
            }
        }

        return $this;
    }

    /**
     * @param $array
     * @return false|string
     */
    protected function getDate($array)
    {

        // Проверим на соответсвие дате проверяем по году.
        $pos = strpos($array[1], $this->year);

        if ($pos === false) {
            return date("Y-m-d");
        }

        $d = explode(' ', $array[1]);

        return date("Y-m") . '-' . $d[0];

    }

    /**
     * @param $html
     * @return array
     */
    protected function getArrayToHtml($html)
    {

        $this->crawler->addHtmlContent($html, 'UTF-8');

        $crawler = $this->crawler
            ->filter('#td-outer-wrap > div:nth-child(2) > div > div.td-pb-row > div.td-pb-span8.td-main-content > div > div.td-page-content.tagdiv-type > table > tbody > tr')->each(function (Crawler $node, $i) {
                return $node->text();
            });

        return $crawler;

    }


    /**
     *
     * Здесь уберем все лишние данные
     *
     * @param array $array
     * @param array $symbols
     * @return array
     */
    protected function clearArray(array $array, array $symbols = array(''))
    {
        return array_diff($array, $symbols);
    }

    /**
     * @return mixed
     */
    public function getSchedules()
    {
        return $this->schedules;
    }
}