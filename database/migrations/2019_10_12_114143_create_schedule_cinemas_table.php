<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateScheduleCinemasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schedule_cinemas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('date');
            $table->time('start_session');
            $table->string('name', 100);
            $table->integer('price');
            $table->string('age_limit', 4)->default('0+');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schedule_cinemas');
    }
}
