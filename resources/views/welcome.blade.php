<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Расписание Сеансов</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <link href="{{asset('/css/app.css')}}" rel="stylesheet">

    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <div class="content">
                <div class="title m-b-md">
                    Расписание Сеансов
                </div>

                <div class="links">
                    <a href="/get">Посмотреть расписание</a>
                    <a href="/update">Обновить расписание</a>
                </div>
            </div>
        </div>
    </body>
</html>
