<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Расписание Сеансов</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    <link href="{{asset('/css/app.css')}}" rel="stylesheet">
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-12">
            <h1 class="text-center">Расписание Сеансов</h1>
        </div>
        <div class="col-12 text-center">
            <div class="links">
                <a href="/get">Посмотреть расписание</a>
                <a href="/update">Обновить расписание</a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <form class="mt-5 mb-5" action="/get">
                <div class="form-row">
                    <div class="col">
                        <label for="inputEmail4">Дата</label>
                        <input type="date" class="form-control" name="date" value="{{ request()->date ?? '' }}">
                    </div>
                    <div class="col">
                        <label for="inputEmail4">Время от</label>
                        <input type="time" class="form-control" name="timeTo" value="{{ request()->timeTo ?? '' }}">
                    </div>
                    <div class="col">
                        <label for="inputEmail4">Время до</label>
                        <input type="time" class="form-control" name="timeDo" value="{{ request()->timeDo ?? '' }}">
                    </div>
                    <div class="col">
                        <label for="inputEmail4">Цена от</label>
                        <input type="number" class="form-control" name="priceTo" value="{{ request()->priceTo ?? '' }}">
                    </div>
                    <div class="col">
                        <label for="inputEmail4">Цена до</label>
                        <input type="number" class="form-control" name="priceDo" value="{{ request()->priceDo ?? '' }}">
                    </div>
                </div>
                <button type="submit" class="btn btn-primary mt-1" name="filter">Применить</button>
                <a href="/get" class="btn btn-danger mt-1" name="reset">Сбросить</a>
            </form>
            @if(!$schedules->isEmpty())
                <table class="table">
                    <thead class="thead-dark">
                    <tr>
                        <th scope="col">Дата</th>
                        <th scope="col">Начало сеанса</th>
                        <th scope="col">Название</th>
                        <th scope="col">Цена</th>
                        <th scope="col">Возраст</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($schedules as $schedule)
                        <tr>
                            <th scope="row">{{ $schedule->date }}</th>
                            <td>{{ $schedule->start_session }}</td>
                            <td>{{ $schedule->name }}</td>
                            <td>{{ $schedule->price }}</td>
                            <td>{{ $schedule->age_limit }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @else
                <div class="alert alert-warning" role="alert">
                    По вашему запросу не чего не найдено!!!
                </div>
            @endif
        </div>
    </div>
</body>
</html>
