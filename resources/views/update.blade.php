<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Расписание Сеансов</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <link href="{{asset('/css/app.css')}}" rel="stylesheet">


</head>
<body>
<div class="flex-center position-ref full-height">
    <div class="content">
        <div class="title m-b-md">
            Расписание Сеансов
        </div>

        <div class="links">
            <a href="/get">Посмотреть расписание</a>
            <a href="/update">Обновить расписание</a>
        </div>
        <div class="row">
            <div class="col-12 mt-3 mb-3"><h3>Результаты обновления</h3></div>
            <div class="col-6">Добавлено записей {{ $countI }}</div>
            <div class="col-6">Пропущено записей {{ $countF }}</div>
        </div>
    </div>
</div>
</body>
</html>